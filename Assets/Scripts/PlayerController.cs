﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    // Unity Accessible fields
    public float Speed = 5;
    public float Gravity = 5;

    // Attached Components
    private CharacterController cc;
    private NetworkView nv;

    // TODO: Move to Input Class
    private const string HorizontalAxis = "Horizontal";
    private const string VerticalAxis = "Vertical";

    // Use this for initialization
    protected void Start () {
        cc = GetComponent<CharacterController> ();
        nv = GetComponent<NetworkView> ();
    }

    // Update is called once per frame
    protected void Update () {
        if (nv.isMine) {
            float horz = Input.GetAxis (HorizontalAxis);
            float vert = Input.GetAxis (VerticalAxis);
            Vector3 moveVec = new Vector3 (horz * Speed, -Gravity, vert * Speed) * Time.deltaTime;
            cc.Move (moveVec);
        } else {
            this.enabled = false;
        }
    }
}
