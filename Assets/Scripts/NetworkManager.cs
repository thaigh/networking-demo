﻿using UnityEngine;
using System;
using System.Collections;

public class NetworkManager : MonoBehaviour {

    // Unity Inspector Public Variables
    public GameObject PlayerPrefab = null;
    public Transform SpawnObject = null;

    private Rect startServerButton;
    private Rect refreshHostsButton;
    private bool refreshingHostData = false;
    private HostData[] hostData = new HostData[0];

    private const int NumberOfAvailableConnections = 2;
    private const int PortNumber = 25576;
    private const string GameId = "b5c08f7d-74de-489d-9ac7-a6e13eca713a";

    private void StartServer()
    {
        bool hasPublicAddress = Network.HavePublicAddress ();
        Debug.Log ("Starting Server " + (hasPublicAddress ? "with public address" : "with NAT"));
        Network.InitializeServer (NumberOfAvailableConnections, PortNumber, !hasPublicAddress);
        MasterServer.RegisterHost (GameId, "Networking-Demo", "Welcome bitchez");
    }

    private void OnServerInitialized()
    {
        Debug.Log ("Server has initialised");
        SpawnPlayer ();
    }

    private void OnConnectedToServer()
    {
        SpawnPlayer ();
    }

    private void SpawnPlayer()
    {
        Network.Instantiate (PlayerPrefab, SpawnObject.position, Quaternion.identity, 0);
    }

    private void OnMasterServerEvent(MasterServerEvent evnt)
    {
        if (evnt == MasterServerEvent.RegistrationSucceeded)
            Debug.Log ("Master Server registration successful");
    }

    private void RefreshHosts()
    {
        Debug.Log ("Refreshing Hosts");
        MasterServer.RequestHostList (GameId);
        refreshingHostData = true;
    }

    // Use this for initialization
    protected void Start () {
        int screenWidth = Screen.width;
        //var screenHeight = Screen.height;
        startServerButton = new Rect (screenWidth * 0.05f, screenWidth * 0.05f, screenWidth * 0.1f, screenWidth * 0.1f);

        refreshHostsButton = new Rect (startServerButton.x,
            (float)(startServerButton.y * 1.2 + startServerButton.height),
            screenWidth * 0.1f, screenWidth * 0.1f);
    }

    // Update is called once per frame
    protected void Update () {
        if (refreshingHostData && MasterServer.PollHostList().Length > 0)
        {
            hostData = MasterServer.PollHostList ();
            Debug.LogFormat ("Found {0} hosts", hostData.Length);
            refreshingHostData = false;
        }
    }

    protected void OnGUI() {

        if (!Network.isServer && !Network.isClient) {
            if (GUI.Button (startServerButton, "Start Server"))
                StartServer ();

            if (GUI.Button (refreshHostsButton, "Refreshing Hosts"))
                RefreshHosts ();

            for (int i = 0; i < hostData.Length; i++) {
                Rect joinButton = new Rect (
                    (float)(startServerButton.x * 1.2 + startServerButton.width),
                    (float)(startServerButton.y * 1.2 + (startServerButton.height * i)),
                    startServerButton.width * 3,
                    startServerButton.height
                );

                if (GUI.Button (joinButton, hostData [i].gameName)) {
                    // Join Game
                    Network.Connect (hostData [i]);
                }
            }
        }
    }


}
