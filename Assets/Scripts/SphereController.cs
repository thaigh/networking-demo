﻿using UnityEngine;
using System.Collections;

public class SphereController : MonoBehaviour {

    // Attached Components
    private Renderer _renderer;
    private NetworkView _networkView;


    // Use this for initialization
    protected void Start () {
        this._renderer = GetComponent<Renderer> ();
        this._networkView = GetComponent<NetworkView> ();
    }

    // Update is called once per frame
    protected void Update () { }

    protected void OnTriggerEnter()
    {
        Debug.Log ("Sphere Trigger entered");
        _networkView.RPC ("SetColourFromVector", RPCMode.AllBuffered, new Vector3(1,0,0));
    }

    [RPC]
    private void SetColourFromVector(Vector3 vec)
    {
        SetColour (new Color (vec.x, vec.y, vec.z, 1));
    }

    private void SetColour(Color c)
    {
        this._renderer.material.color = c;
    }

}
